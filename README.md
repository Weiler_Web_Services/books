![Books Icon](Book.png)

---

# Coding Books


- 101 Ready-to-Use Excel Formulas - Michael Alexander = pdf, epub, & mobi

- A Byte of Python - Swaroop C H = pdf, epub, & mobi

- A Smarter Way to Learn JavaScript - The new approach that uses technology to cut your - effort in half - Myers = pdf, epub, & mobi

- A Whirlwind Tour of Python - Jake VanderPlas = pdf & mobi

- Advanced Programming in the UNIX Environment 3rd Edition = pdf, mobi

- An Introduction to pyttsx3 - A Text-To-Speech Converter for Python = pdf

- Automate the Boring Stuff with Python = pdf, epub, & mobi

- Bash Cookbook = pdf

- Beginning Game Programming for Teens with Python = pdf, epub, & mobi

- Beginning Linux Programming 4th Edition = pdf, epub, & mobi

- Beginning Perl = pdf

- Beginning PHP And MySQL From Novice To Professional 4th Edition = pdf, epub, & mobi

- Binary Hacks = pdf, epub, & mobi

- Brain Training - How To Learn and Remember Everything, Increase memory (How To Remember - Book) - George Lynch = pdf, epub, & mobi

- Build a Python Flask App on Glitch! = pdf, epub, & mobi

- Build Your Own Database Driven Website using PHP & MySQL - Kevin Yank = pdf, epub, &  mobi

- Chrome DevTools Keyboard Shortcuts = pdf, epub, & mobi

- Code - The Hidden Language of Computer Hardware and Software - Charles Petzold = pdf,  epub, & mobi

- Coding & English Lit- Natural Language Processing in Python = pdf, epub, & mobi

- Coding & World Language Lit- Analyzing Dante’s Inferno with Python NLTK = pdf, epub, &  mobi

- Coding for Kids - Tynker = mobi & epub 

- Computer Coding Python Projects for Kids - Carol Vorderman = pdf, epub, & mobi

- Computer Games and ESL Learning = pdf, epub, & mobi

- Core Python Applications Programming - Wesley J. Chun = pdf, epub, & mobi

- Cracking Codes with Python - Al Sweigart = pdf, epub, & mobi

- Debug Hacks = pdf, epub, & mobi

- Deep Learning with Python - A Hands-on Introduction = pdf, epub, & mobi

- Developer Productivity Engineering = pdf, epub, & mobi

- Development of an Adventure Game = pdf

- Django - Django Software Foundation = pdf, epub, & mobi

- Django Documentation = pdf, epub

- DjVu Document - Chuan = pdf, epub, & mobi

- DroidScript Tutorials - DroidScript = pdf, epub, & mobi

- Effective Python - 59 Specific Ways to Write Better Effective Software Development  Series - - Brett Slatkin = pdf, epub, & mobi

- Eloquent JavaScript = pdf, epub, & mobi

- Everything curl - bagder = pdf, epub, & mobi

- Excel 2019 Power Programming with VBA - Michael Alexander = pdf, epub, & mobi

- Expert PHP and MySQL - Marc Rochkind = pdf, epub, & mobi

- Fluent Python - Luciano Ramalho = pdf, epub, & mobi

- Getting Started With Python IDLE = pdf, epub, & mobi

- GNU Gnulib = pdf

- Hacking Secret Ciphers with Python - Al Sweigart = pdf, epub, & mobi

- High Availability MySQL Cookbook Over 50 Simple But Incredibly Effective Recipes- High  Availability MySQL Cookbook Over 50 Simple But Incredibly Effective Recipes = epub

- How to Add Background Music to Your Web Page = pdf, epub, & mobi

- How to Think Like a Computer Scientist- Learning with Python 2nd Edition = epub & mobi

- HTMLDOC Users Manual - Michael R Sweet = pdf, epub, & mobi

- Human-centric Computing and Information Sciences = pdf, epub, & mobi

- Impatient Perl = pdf, epub, & mobi

- Install TensorFlow with pip = pdf, epub, & mobi

- Intro to React = pdf, epub, & mobi

- Introduction to Machine Learning with Python - Andreas C. Muller & Sarah Guido = pdf

- Invent Your Own Computer Games with Python 2nd Edition - Al Sweigart = pdf

- Invent Your Own Games with Python 2nd Edition = pdf & epub

- JavaScript MDN Web Docs 2019 = pdf, epub, & mobi

- Learn Enough Command Line to Be Dangerous - Learn Enough to Be Dangerous = pdf

- Learn Python the Hard Way A Very Simple Introduction to the Terrifyingly Beautiful  World - of = pdf

- Learn Ruby the Hard Way = pdf

- Learn to Program with Minecraft- Transform Your World with the Power of Python = pdf

- Learning Bash = pdf

- Learning PHP, MySQL & JavaScript - Robin Nixon = pdf, epub, & mobi

- Learning Python Language = pdf

- Learning Python Powerful Object-Oriented Programming, 5TH EDITION OREILLY = pdf

- Linux System Programming - Robert Love = pdf & epub

- Linux System Programming 2nd Edition = pdf

- Live video streaming with open source Video.js = pdf & epub

- Making Games with Python & Pygame = pdf

- Mastering PHPmyadmin = pdf, epub, & mobi

- Mastering Regular Expressions Powerful Techniques for Perl and Other Tools - Jeffrey  E.F. - Friedl = pdf & epub

- Mastering Regular Expressions Third Edition - Jeffrey E.F. Friedl = pdf, epub, & mobi

- Modern Perl 4th Edition.pdg = pdf, epub, & mobi

- MySQL and PHP = pdf, epub, & mobi

- MySQL Cookbook - Solutions for Database Developers and Administrators - DuBois, Paul = pdf, epub, & mobi

- MySQL for Python - Albert Lukaszewski = pdf, epub, & mobi

- MySQL for the Internet of Things - Charles Bell = pdf, epub, & mobi

- MySQL Language Reference = pdf, epub, & mobi

- OpenSSL Cookbook - Ivan Ristic = pdf, epub, & mobi

- PHP & MySQL - Novice to Ninja, 6th Edition = pdf, epub, & mobi

- PHP and MySQL for Dynamic Web Sites - Larry Ullman = pdf, epub, & mobi

- PHP, MySQL, & JavaScript All-in-One For Dummies = pdf, epub, & mobi

- PHP, MySQL, JavaScript & HTML5 All-In-One For Dummies - Steven Suehring, Janet - Valade = pdf, epub, & mobi

- PHP5 and MySQL Bible - Tim Converse, Joyce Park, Clark Morgan = pdf, epub, & mobi

- PostgreSQL- Up and Running 3rd Edition - Regina O. Obe = pdf, epub, & mobi

- Practical PHP and MySQL Website Databases - A Simplified Approach - Adrian W. West = 
 pdf, epub, & mobi

- Practical SQL - Anthony DeBarros = pdf, epub, & mobi

- Pro Git Book = pdf, epub, & mobi

- Professional Perl Programming = pdf, epub, & mobi

- Programming Google App Engine with Python - Dan Sanderson = pdf, epub, & mobi

- Programming Perl 4th Edition - Tom Christiansen = pdf, epub, & mobi

- Python All-in-One For Dummies - John Shovic & Alan Simpson = pdf

- Python Cookbook - David Beazley & Brian K. Jones = pdf

- Python for Everybody - Charles Severance = pdf, epub, & mobi

- Python IDEs and Code Editors (Guide) = pdf, epub, & mobi

- Python Packaging User Guide = pdf

- Python Pocket Reference - Mark Lutz = pdf

- Python Programming- An Introduction to Computer Science, 3rd Edition = pdf

- Python Simple HTTP Server - A Simple HTTP Web Server With Python = pdf

- Quick Start Guide - John Schember = epub

- Ruby Installer for Windows = pdf, epub, & mobi

- Scratch Programming Playground = pdf, epub, & mobi

- Scrum - The Art of Doing Twice the Work in Half the Time - Jeff Sutherland = pdf, epub, & mobi

- Some Simple & Amazing JavaScript Tricks = pdf, epub, & mobi

- The bash shell - Linux Shell Scripting Tutorial - A Beginner's handbook = pdf

- The Book of Ruby = pdf

- The easy way to install Ruby on Windows = pdf, epub, & mobi

 The Linux Command Line - A Complete Introduction - William Shotts = pdf & epub

- The Linux Programming Interface - A Linux and UNIX System Programming Handbook -- Michael Kerrisk = pdf, epub, & mobi

- Theory- Introduction to Python = pdf

- Tortoise SVN Tutorial - Aalap Tripathy = pdf, epub, & mobi

- Tortoise SVN Tutorial - Subversion Configuration, Access, Basic Operations, - Troubleshooting, Additional Reading = pdf

- Unix Network Programming Vol 2, Second Edition - W. Richard Stevens = pdf

- Unix Network Programming Volume 1, Third Edition - The Sockets Networking API = pdf

- WROX - ISBN 1861003145 - Beginning Perl = epub & mobi

---


# Kids Books

## Diary of a Wimpy Kid - Jeff Kinney:

- 1 Diary of a Wimpy Kid = epub & mobi

- 2 Rodrick Rules = epub & mobi

- 3 The Last Straw = epub & mobi

- 4 Dog Days = epub & mobi

- 5 The Ugly Truth = epub & mobi

- 6 Cabin Fever = epub & mobi

- 7 The Third Wheel = epub & mobi

- 8 Hard Luck = epub & mobi

- 9 The Long Haul = epub & mobi

- 10 Old School = epub & mobi

- 11 Double Down by Jeff Kinney = epub & mobi

## Dr. Seuss:

- 500 Hats of Bartholomew Cubbins = epub & mobi

- And to Think That I Saw It on Mulberry Street = pdf, epub, & mobi

- Bartholomew and the Oobleck = pdf, epub, & mobi

- Horton Hatches the Egg = pdf, epub, & mobi

- How the Grinch Stole Christmas! = pdf, epub, & mobi

- I Had Trouble in Getting to Solla Sollew = pdf, epub, & mobi

- If I Ran the Circus = pdf, epub, & mobi

- If I Ran the Zoo = pdf, epub, & mobi

- McElligot's Pool = pdf, epub, & mobi

- Oh, the Places You'll Go! = pdf, epub, & mobi

- On Beyond Zebra! = pdf, epub, & mobi

- Scrambled Eggs Super! = pdf, epub, & mobi

- The 500 Hats of Bartholomew Cubbins = pdf, epub, & mobi

- The Cat in the Hat = pdf, epub, & mobi

- The King's Stilts = pdf, epub, & mobi

- The Lorax = pdf, epub, & mobi

- The Seven Lady Godivas = pdf, epub, & mobi

- The Sneetches - Dr. Seuss = pdf, epub, & mobi

- Thidwick the Big-Hearted Moose = pdf, epub, & mobi

## Gingerbread Man:

- Gingerbread Man - Bev Evans = pdf, epub, & mobi

- The Little Gingerbread Man - Carol Moore = pdf, epub, & mobi


## Harry Potter - J.K. Rowling:

- Harry Potter and the Cursed Child = pdf, epub, & mobi


---
